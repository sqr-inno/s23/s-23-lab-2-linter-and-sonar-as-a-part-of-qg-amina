# Lab 2 -- Linter and SonarQube as a part of quality gates

[![pipeline status](https://gitlab.com/sqr-inno/s23/s-23-lab-2-linter-and-sonar-as-a-part-of-qg-amina/badges/master/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-2-linter-and-sonar-as-a-part-of-qg-amina/-/commits/master)

## Homework

Screens:

![img1](images/img1.png)
![img2](images/img2.png)
![img3](images/img3.png)
![img4](images/img4.png)
